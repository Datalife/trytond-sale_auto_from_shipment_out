# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import unittest
import doctest
from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import suite as test_suite
from trytond.tests.test_tryton import doctest_teardown, doctest_checker


class SaleAutoFromShipmentOutTestCase(ModuleTestCase):
    """Test Sale Auto From Shipment Out module"""
    module = 'sale_auto_from_shipment_out'


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            SaleAutoFromShipmentOutTestCase))
    suite.addTests(doctest.DocFileSuite(
            'scenario_sale.rst',
            tearDown=doctest_teardown, encoding='utf-8',
            checker=doctest_checker,
            optionflags=doctest.REPORT_ONLY_FIRST_FAILURE))
    return suite
