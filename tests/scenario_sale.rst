====================================
Sale Auto From Shipment Out Scenario
====================================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from operator import attrgetter
    >>> from proteus import Model, Wizard, Report
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences, create_payment_term
    >>> today = datetime.date.today()

Install sale_auto_from_shipment_out::

    >>> config = activate_modules('sale_auto_from_shipment_out')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Reload the context::

    >>> User = Model.get('res.user')
    >>> Group = Model.get('res.group')
    >>> config._context = User.get_preferences(True, config.context)

Create sale user::

    >>> sale_user = User()
    >>> sale_user.name = 'Sale'
    >>> sale_user.login = 'sale'
    >>> sale_user.company = company
    >>> sale_group, = Group.find([('name', '=', 'Sales')])
    >>> sale_user.groups.append(sale_group)
    >>> sale_user.save()

Create stock user::

    >>> stock_user = User()
    >>> stock_user.name = 'Stock'
    >>> stock_user.login = 'stock'
    >>> stock_user.company = company
    >>> stock_group, = Group.find([('name', '=', 'Stock')])
    >>> stock_user.groups.append(stock_group)
    >>> stock_user.save()

Create account user::

    >>> account_user = User()
    >>> account_user.name = 'Account'
    >>> account_user.login = 'account'
    >>> account_user.company = company
    >>> account_group, = Group.find([('name', '=', 'Account')])
    >>> account_user.groups.append(account_group)
    >>> account_user.save()

Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']
    >>> cash = accounts['cash']

Create tax::

    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()

Create account categories::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.save()

    >>> account_category_tax, = account_category.duplicate()
    >>> account_category_tax.customer_taxes.append(tax)
    >>> account_category_tax.save()

Create parties::

    >>> Party = Model.get('party.party')
    >>> supplier = Party(name='Supplier')
    >>> supplier.save()
    >>> customer = Party(name='Customer')
    >>> customer.save()

Create category::

    >>> ProductCategory = Model.get('product.category')
    >>> category = ProductCategory(name='Category')
    >>> category.save()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> product = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.list_price = Decimal('10')
    >>> template.cost_price = Decimal('5')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> product.template = template
    >>> product.save()

    >>> service = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'service'
    >>> template.default_uom = unit
    >>> template.type = 'service'
    >>> template.salable = True
    >>> template.list_price = Decimal('30')
    >>> template.cost_price = Decimal('10')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> service.template = template
    >>> service.save()

Create payment term::

    >>> payment_term = create_payment_term()
    >>> payment_term.save()
    >>> customer.customer_payment_term = payment_term
    >>> customer.save()

Create an Inventory::

    >>> config.user = stock_user.id
    >>> Inventory = Model.get('stock.inventory')
    >>> Location = Model.get('stock.location')
    >>> storage, = Location.find([
    ...         ('code', '=', 'STO'),
    ...         ])
    >>> inventory = Inventory()
    >>> inventory.location = storage
    >>> inventory_line = inventory.lines.new()
    >>> inventory_line.product = product
    >>> inventory_line.quantity = 100.0
    >>> inventory_line.expected_quantity = 0.0
    >>> inventory.click('confirm')
    >>> inventory.state
    'done'

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> supplier_loc, = Location.find([('code', '=', 'SUP')])
    >>> customer_loc, = Location.find([('code', '=', 'CUS')])
    >>> output_loc, = Location.find([('code', '=', 'OUT')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])
    >>> input_loc, = Location.find([('code', '=', 'IN')])

Ship 5 products::

    >>> Shipment = Model.get('stock.shipment.out')
    >>> Move = Model.get('stock.move')
    >>> shipment = Shipment()
    >>> shipment.effective_date = today
    >>> shipment.customer = customer
    >>> shipment.warehouse = warehouse_loc
    >>> move_out = Move()
    >>> shipment.outgoing_moves.append(move_out)
    >>> move_out.product = product
    >>> move_out.quantity = 2.0
    >>> move_out.unit_price = Decimal('10')
    >>> move_out.from_location = output_loc
    >>> move_out.to_location = customer_loc
    >>> move_out = Move()
    >>> shipment.outgoing_moves.append(move_out)
    >>> move_out.from_location = storage
    >>> move_out.product = product
    >>> move_out.quantity = 3.0
    >>> move_out.unit_price = Decimal('10')
    >>> move_out.from_location = output_loc
    >>> move_out.to_location = customer_loc
    >>> shipment.save()
    >>> shipment.click('wait')
    >>> shipment.click('assign_try')
    True

Check sale::

    >>> admin, = User.find([('login', '=', 'admin')])
    >>> config.user = admin.id
    >>> shipment.state
    'assigned'
    >>> shipment.click('pick')
    >>> shipment.state
    'picked'
    >>> shipment.click('pack')
    >>> shipment.state
    'packed'
    >>> Sale = Model.get('sale.sale')
    >>> Conf = Model.get('sale.configuration')
    >>> conf = Conf(1)
    >>> sale, = Sale.find([])
    >>> sale.shipments[0].id == shipment.id
    True
    >>> len(sale.lines)
    2
    >>> sum(l.quantity for l in sale.lines)
    5.0
    >>> sale.state
    'processing'
    >>> len(sale.invoices)
    0
    >>> sale.untaxed_amount, sale.tax_amount, sale.total_amount
    (Decimal('50.00'), Decimal('5.00'), Decimal('55.00'))
    >>> sale.save()

Finish shipment::

    >>> shipment.click('done')
    >>> shipment.state
    'done'
    >>> sale.reload()
    >>> len(sale.invoices)
    1
    >>> invoice = sale.invoices[0]
    >>> len(invoice.lines)
    2
    >>> len(invoice.lines[0].stock_moves)
    1
    >>> invoice.lines[0].stock_moves[0].shipment.id == shipment.id
    True

Cancel sale::

    >>> shipment.click('cancel_auto_sale')
    >>> shipment.reload()
    >>> shipment.state = 'draft'
    >>> list(set(m.state for m in shipment.moves))
    ['draft']
    >>> sale = Sale.find([])
    >>> not sale
    True
    >>> Invoice = Model.get('account.invoice')
    >>> invoice = Invoice.find([])
    >>> not invoice
    True

Check restrictions::

    >>> shipment.click('wait')
    >>> shipment.click('assign_try')
    True
    >>> shipment.click('pick')
    >>> shipment.click('pack')
    >>> shipment.click('done')
    >>> sale, = Sale.find([])
    >>> invoice = sale.invoices[0]
    >>> invoice.click('validate_invoice')
    >>> shipment.click('cancel_auto_sale') # doctest: +ELLIPSIS
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Cannot cancel automatic Sale of Customer Shipment "..." due to invoicing state. - 

Return some products::

    >>> ShipmentReturn = Model.get('stock.shipment.out.return')
    >>> Move = Model.get('stock.move')
    >>> shipment_return = ShipmentReturn()
    >>> shipment_return.effective_date = today
    >>> shipment_return.customer = customer
    >>> shipment_return.warehouse = warehouse_loc
    >>> move_in = Move()
    >>> shipment_return.incoming_moves.append(move_in)
    >>> move_in.product = product
    >>> move_in.quantity = 2.0
    >>> move_in.unit_price = Decimal('10')
    >>> move_in.from_location = customer_loc
    >>> move_in.to_location = input_loc
    >>> shipment_return.save()
    >>> shipment_return.click('receive')
    >>> sale_return, = Sale.find([('shipment_returns', '=', shipment_return.id)])
    >>> len(sale_return.lines)
    1
    >>> sale_return.lines[0].quantity
    -2.0
    >>> sale_return.untaxed_amount, sale_return.tax_amount, sale_return.total_amount
    (Decimal('-20.00'), Decimal('-2.00'), Decimal('-22.00'))
    >>> shipment_return.click('done')
    >>> sale_return.reload()
    >>> sale_return.state
    'processing'

Test salable check::

    >>> customer.sale_from_shipment = False
    >>> customer.save()
    >>> shipment = Shipment()
    >>> bool(shipment.salable)
    True
    >>> shipment.effective_date = today
    >>> shipment.customer = customer
    >>> bool(shipment.salable)
    False
    >>> shipment.warehouse = warehouse_loc
    >>> move_out = Move()
    >>> shipment.outgoing_moves.append(move_out)
    >>> move_out.product = product
    >>> move_out.quantity = 2.0
    >>> move_out.unit_price = Decimal('10')
    >>> move_out.from_location = output_loc
    >>> move_out.to_location = customer_loc
    >>> shipment.save()
    >>> shipment.click('wait')
    >>> shipment.click('pack')
    >>> shipment.reload()
    >>> not shipment.outgoing_moves[0].origin
    True
    >>> len(Sale.find([]))
    2
    >>> len(Invoice.find([]))
    2