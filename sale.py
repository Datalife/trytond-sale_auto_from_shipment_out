# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, fields
from trytond.pool import PoolMeta
from trytond.pyson import Bool, Eval
from trytond.modules.company.model import CompanyValueMixin
from trytond.tools.multivalue import migrate_property


class Configuration(metaclass=PoolMeta):
    __name__ = 'sale.configuration'

    sale_auto_state = fields.MultiValue(
        fields.Selection([('processing', 'Processing'), ('done', 'Done')],
            'Sale auto state',
            states={'required': Bool(Eval('context', {}).get('company'))}))

    @classmethod
    def default_sale_auto_state(cls, **pattern):
        return cls.multivalue_model(
            'sale_auto_state').default_sale_auto_state()


class ConfigurationState(ModelSQL, CompanyValueMixin):
    "Configuration State"
    __name__ = 'sale.configuration.sale_auto_state'

    sale_auto_state = fields.Selection([('processing', 'Processing'),
                          ('done', 'Done')], 'Sale auto state',
                    states={'required': Bool(
                        Eval('context', {}).get('company'))})

    @classmethod
    def default_sale_auto_state(cls):
        return 'processing'

    @classmethod
    def __register__(cls, module_name):
        table_h = cls.__table_handler__(module_name)
        exist = table_h.table_exist(cls._table)

        super(ConfigurationState, cls).__register__(module_name)

        if not exist:
            cls._migrate_property([], [], [])

    @classmethod
    def _migrate_property(cls, field_names, value_names, fields):
        field_names.append('sale_auto_state')
        value_names.append('sale_auto_state')
        fields.append('company')
        migrate_property('sale.configuration',
            field_names, cls, value_names, fields=fields)


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    @classmethod
    def __setup__(cls):
        super(Sale, cls).__setup__()
        cls._transitions |= set((
            ('done', 'cancelled'),
            ('processing', 'cancelled')))

    def _group_shipment_key(self, moves, move):
        res = super()._group_shipment_key(moves, move)
        return res + (('salable', False), )

    def _group_return_key(self, moves, move):
        res = super()._group_return_key(moves, move)
        return res + (('salable', False), )
