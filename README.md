datalife_sale_auto_from_shipment_out
====================================

The sale_auto_from_shipment_out module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-sale_auto_from_shipment_out/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-sale_auto_from_shipment_out)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
