# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Bool
from trytond.transaction import Transaction
from trytond.i18n import gettext
from trytond.exceptions import UserError


class SaleAutoMixin(object):

    salable = fields.Boolean('Salable',
        states={
            'readonly': Bool(Eval('sales'))
        },
        depends=['sales'],
        help='If checked generates Sale if not exists.')
    sales = fields.Function(
        fields.Many2Many('sale.sale', None, None, 'Sales'),
        'get_sales')

    @staticmethod
    def default_salable():
        return True

    @classmethod
    def create_auto_sale(cls, records):
        pool = Pool()
        Sale = pool.get('sale.sale')
        SaleLine = pool.get('sale.line')
        Configuration = pool.get('sale.configuration')

        conf = Configuration(1)
        vlist = []
        taxes = {}
        sale_state = (conf.sale_auto_state
            or Configuration.default_sale_auto_state())
        _move_type = ('incoming_moves'
            if cls.__name__.endswith('.return') else 'outgoing_moves')
        for shipment in records:
            if not shipment.salable:
                continue
            sale_lines = []
            moves = getattr(shipment, _move_type, [])
            _sale = shipment._get_auto_sale(sale_state)
            party = _sale['party']
            for move in moves:
                if move.origin is not None:
                    continue
                if taxes.get((party.id, move.product.id), None) is None:
                    taxes.setdefault((party.id, move.product.id), [])
                    pattern = SaleLine()._get_tax_rule_pattern()
                    for tax in move.product.template.customer_taxes_used:
                        if party and party.customer_tax_rule:
                            tax_ids = party.customer_tax_rule.apply(tax,
                                pattern)
                            if tax_ids:
                                taxes[(party.id, move.product.id)].extend(
                                    tax_ids)
                            continue
                        taxes[(party.id, move.product.id)].append(tax.id)
                    if party and party.customer_tax_rule:
                        tax_ids = party.customer_tax_rule.apply(None, pattern)
                        if tax_ids:
                            taxes[(party.id, move.product.id)].extend(tax_ids)

                _line = shipment._get_auto_sale_line(
                    move, taxes[(party.id, move.product.id)])
                if _line:
                    sale_lines.append(_line)
            if sale_lines:
                _sale['lines'] = [('create', sale_lines)]
                vlist.append(_sale)

        if vlist:
            sales = Sale.create(vlist)
            Sale.set_number(sales)
            Sale.store_cache(sales)
            [s.set_shipment_state() for s in sales]

    @classmethod
    @ModelView.button
    def cancel_auto_sale(cls, records):
        pool = Pool()
        Invoice = pool.get('account.invoice')
        InvoiceLine = pool.get('account.invoice.line')
        Sale = pool.get('sale.sale')
        Move = pool.get('stock.move')
        IrModel = Pool().get('ir.model')

        model, = IrModel.search([('model', '=', cls.__name__)])
        sales = []
        invoice_lines = []
        _shipment_type = ('shipment_returns'
            if cls.__name__.endswith('.return') else 'shipments')
        for record in records:
            if not record.salable:
                continue
            all_sales = list(record.sales)
            if not all_sales:
                continue
            if len(all_sales) > 1:
                raise UserError(gettext(
                    'sale_auto_from_shipment_out.msg_auto_sale_shipments',
                    model=model.name,
                    record=record.rec_name))
            sale, = all_sales
            if any(l.invoice_lines for l in sale.lines):
                if (sale.invoice_state not in ('none', 'waiting')
                    or len(sale.invoices) > 1
                    or (sale.invoices
                        and sale.invoices[0].state not in ('draft', 'cancelled'))):
                    raise UserError(gettext(
                        'sale_auto_from_shipment_out.msg_auto_sale_invoices',
                        model=model.name,
                        record=record.rec_name))

                invoice_lines.extend(list(
                    set(i for l in sale.lines for i in l.invoice_lines)))
            if len(getattr(sale, _shipment_type, [])) > 1:
                raise UserError(gettext(
                    'sale_auto_from_shipment_out.msg_auto_sale_shipments',
                    model=model.name,
                    record=record.rec_name))

            sales.append(sale)

        # consider invoice grouping
        if invoice_lines:
            InvoiceLine.write(invoice_lines, {'origin': None})

        if sales:
            Sale.cancel(sales)
            Sale.delete(sales)

        if invoice_lines:
            invoices = set([l.invoice for l in invoice_lines if l.invoice])
            InvoiceLine.delete(invoice_lines)

            if invoices:
                lines = InvoiceLine.search([
                    ('invoice', 'in', list(map(int, invoices))),
                    ('type', '=', 'line')])
                if not lines:
                    Invoice.delete(invoices)
        with Transaction().set_context(check_origin=False,
                check_shipment=False):
            Move.write([move for move in record.moves for record in records],
                {'origin': None})
            cls.cancel(records)
            cls.draft(records)

    def _get_auto_sale(self, _state):
        return {
            'company': self.company,
            'description': self.number,
            'state': _state,
            'sale_date': self.effective_date,
            'payment_term': self.customer.customer_payment_term,
            'party': self.customer,
            'invoice_address': self.customer.address_get('invoice'),
            'shipment_address': self.delivery_address,
            'warehouse': self.warehouse,
            'comment': self.reference,
            'reference': self.reference,
            'shipment_method': 'order',
            'invoice_method': 'shipment'
        }

    def _get_auto_sale_line(self, move, taxes):
        return {
            'type': 'line',
            'quantity': move.quantity,
            'unit': move.uom,
            'product': move.product,
            'unit_price': move.unit_price,
            'description': move.product.rec_name,
            'taxes': [('add', taxes)],
            'moves': [('add', [move.id])],
        }

    @fields.depends('customer')
    def on_change_customer(self):
        super().on_change_customer()
        _field = 'sale_from_shipment'
        if self.__name__.endswith('.return'):
            _field += '_return'
        if self.customer:
            self.salable = getattr(self.customer, _field)

    def get_sales(self, name):
        SaleLine = Pool().get('sale.line')

        moves = (self.incoming_moves
            if self.__name__.endswith('.return') else self.outgoing_moves)
        sales = list(set([m.origin.sale.id for m in moves
            if isinstance(m.origin, SaleLine)]))
        return sales


class ShipmentOut(SaleAutoMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.out'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._buttons.update({
            'cancel_auto_sale': {
                'icon': 'tryton-back',
                'invisible': (
                    ~Eval('state').in_(['packed', 'done'])
                    & ~Eval('salable')),
                'depends': ['state', 'salable']},
        })
        cls.outgoing_moves.states['readonly'] |= (
            Eval('state').in_(['assigned', 'packed']))
        cls.salable.states['readonly'] |= ~Eval('state').in_(['draft',
            'waiting', 'assigned'])
        cls.salable.depends.append('state')

    @classmethod
    def copy(cls, records, default=None):
        pool = Pool()
        Move = pool.get('stock.move')

        if default is None:
            default = {}
        default = default.copy()
        default.setdefault('number', None)
        shipments = super(ShipmentOut, cls).copy(records, default=default)
        Move.write([m for s in shipments for m in s.outgoing_moves],
                   {'origin': None})
        return shipments

    @classmethod
    def pack(cls, shipments):
        cls.create_auto_sale(shipments)
        super(ShipmentOut, cls).pack(shipments)


class ShipmentOutReturn(SaleAutoMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.out.return'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._buttons.update({
            'cancel_auto_sale': {
                'icon': 'tryton-back',
                'invisible': (
                    ~Eval('state').in_(['packed', 'done'])
                    & ~Eval('salable')),
                'depends': ['state', 'salable']},
        })
        cls.salable.states['readonly'] |= (Eval('state') != 'draft')
        cls.salable.depends.append('state')

    @classmethod
    def copy(cls, records, default=None):
        pool = Pool()
        Move = pool.get('stock.move')

        if default is None:
            default = {}
        default = default.copy()
        default.setdefault('number', None)
        shipments = super().copy(records, default=default)
        Move.write([m for s in shipments for m in s.incoming_moves],
                   {'origin': None})
        return shipments

    @classmethod
    def receive(cls, shipments):
        cls.create_auto_sale(shipments)
        super().receive(shipments)

    def _get_auto_sale_line(self, move, taxes):
        res = super(ShipmentOutReturn, self)._get_auto_sale_line(move, taxes)
        res['quantity'] *= -1
        return res


class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'

    @fields.depends('from_location', 'to_location',
        '_parent_from_location.type', '_parent_to_location.type')
    def on_change_with_unit_price_required(self, name=None):
        if (self.from_location and self.to_location
                and self.from_location.type == 'customer'
                and self.to_location.type == 'storage'):
            return True
        return super(Move, self).on_change_with_unit_price_required(name)

    @fields.depends('product', 'from_location', '_parent_from_location.type',
        '_parent_product.list_price', 'uom', '_parent_product.default_uom',
        'currency', 'company', '_parent_company.currency')
    def on_change_product(self):
        super(Move, self).on_change_product()
        pool = Pool()
        Uom = pool.get('product.uom')
        Currency = pool.get('currency.currency')

        if (self.product and self.from_location
                and self.from_location.type == 'customer'):
            unit_price = self.product.list_price
            if unit_price:
                if self.uom != self.product.default_uom:
                    unit_price = Uom.compute_price(self.product.default_uom,
                        unit_price, self.uom)
                if self.currency and self.company:
                    unit_price = Currency.compute(self.company.currency,
                        unit_price, self.currency, round=False)
                self.unit_price = unit_price

    @classmethod
    def check_origin_types(cls):
        types = super(Move, cls).check_origin_types()
        types.remove('customer')
        return types
