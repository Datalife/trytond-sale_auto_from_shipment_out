# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .sale import Configuration, Sale, ConfigurationState
from .shipment import ShipmentOut, ShipmentOutReturn, Move
from . import party


def register():
    Pool.register(
        Sale,
        ShipmentOut,
        ShipmentOutReturn,
        Configuration,
        ConfigurationState,
        Move,
        party.Party,
        module='sale_auto_from_shipment_out', type_='model')
